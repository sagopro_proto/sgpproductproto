PROTOC_FILE_MAIN = ./product.proto
PROTOC_FILE_BANNER = ./banner/banner.proto
PROTOC_FILE_PROPERTY_CAT = ./property_category/property_category.proto
PROTOC_FILE_PROPERTY = ./property/property.proto
PROTOC_FILE_PROPERTY_COVER = ./property_cover/property_cover.proto
PROTOC = protoc
PROTOC_FLAGS = -I . --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative
GIT_ALL = git add . && git commit -m "--" && git push origin master

git:
	@echo "git all"
	$(GIT_ALL)



banner:
	@echo "create sgp product banner..."
	$(PROTOC) $(PROTOC_FLAGS) $(PROTOC_FILE_BANNER)
	@echo "create sgp product banner..."

main:
	@echo "create sgp product proto..."
	$(PROTOC) $(PROTOC_FLAGS) $(PROTOC_FILE_MAIN)
	@echo "create sgp product proto..."
	
prop_cat:
	@echo "create sgp prop category proto..."
	$(PROTOC) $(PROTOC_FLAGS) $(PROTOC_FILE_PROPERTY_CAT)

prop:
	@echo "create sgp prop proto..."
	$(PROTOC) $(PROTOC_FLAGS) $(PROTOC_FILE_PROPERTY)

prop_cover:
	@echo "create sgp prop proto..."
	$(PROTOC) $(PROTOC_FLAGS) $(PROTOC_FILE_PROPERTY_COVER)


all: prop_cover prop prop_cat banner main

.PHONY: main prop_cat all git swag banner
